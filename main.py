# Загрузка необходимых библиотек и т.д.
from sqlalchemy import select
from starlette import status
# Лучше не делать import *, плохая практика
from database import Fly, engine, Base, SessionLocal
from sqlalchemy.orm import Session
from fastapi import Depends, FastAPI, Body, HTTPException
from fastapi.responses import FileResponse

from models import FlyRead

# это тоже лучше либо вынести отдельно либо 
#   вообще не вызывать, потому что для миграций есть специальные утилиты типа alembic
# создаем таблицы
Base.metadata.create_all(bind=engine)

app = FastAPI()


# определяем зависимость
# Кстати эту тему лучше вытащить в куда нибудь (например в database.py)
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@app.get("/")
def main():
    return FileResponse("public/index.html")


@app.get("/flies", response_model=list[FlyRead])
def get_flies(db: Session = Depends(get_db)):
    # Это новый синтаксис, лучше использовать его
    flies = db.scalars(select(Fly)).fetchall()
    return flies
    


def valid_fly(fly_id: int, db: Session = Depends(get_db)) -> Fly:
    fly = db.get(Fly, fly_id)
    if fly is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="Flight not found")  # Есть замечательная сущность для явного указания сообщений
    return fly


@app.get("/flies/{fly_id}", response_model=FlyRead)
# валидацию айдишников тоже никто не отменял
# лучше по-разному называть разные роуты
# и получение сущности бд по айдишнику можно сделать тоже через depends, следуя best practices, которые есть в беседке
def get_fly(fly: Fly = Depends(valid_fly)):
    # получаем рейс по id
    # fly = db.query(Fly).filter(Fly.id == id).first()
    # По id можно получать немного другим способом
    # fly = db.get(Fly, fly_id)
    # если не найден, отправляем статусный код и сообщение об ошибке
    # if fly is None:
    # status_code лучше не хардкодить, а использовать переменные
    # return JSONResponse(status_code=status.HTTP_404_NOT_FOUND, content={"message": "Рейс не найден"})
    # если рейс найден, отправляем его

    # Тут может быть проблема: пользователю например не стоит знать id каждой сущности,
    #   лишний раз пусть такая информация не утекает, поэтому можно сделать response_model в атрибутах декоратора
    return fly


@app.get("/companies", response_model=list[str])
def get_companies(db: Session = Depends(get_db)):
    # Тут удобнее использовать синтаксис новой алхимии,
    #   конструкция unique(...) позволяет отфильтровать неуникальные компании
    return db.execute(select(Fly.company)).unique(lambda fly: fly.company).fetchall()


# Функция поиска (фильтрации по параметрам)
# Эту функцию лучше тогда либо выделить в отдельный роут, либо вообще не заводить
# @app.post("/flies")
# def search_flies(data=Body(), db: Session = Depends(get_db)):
#     # Соответственно, тут надо обдумать, как именно будет фильтроваться полёт
#     #   (конечно, не обязательно и дополнительный гемор, тем не менее)
#     fly = db.query(Fly).filter(Fly.departure == data["departure"]).filter(Fly.arrival == data["arrival"]).filter(
#         Fly.day == data["day"]).all()
#     return fly
