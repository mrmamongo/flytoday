from pydantic import BaseModel

# Данная модель является так называемым 
#   DTO - data transfer object. Это такой тип объектов,
#   с помощью которого производится общение между разнородными приложениями.
#   Данный формат удобен (или скорее необходим) в случаях, 
#   когда идёт общение между разными приложениями (чаще всего написанными на разных языках)
#   В данном случае используется (в конце концов) для сериализации протокол json
class FlyRead(BaseModel):
    departure: str
    arrival: str
    day: int
    company: str
    econom: int
    busines: int

    class Config:
        orm_mode = True