import enum

from sqlalchemy import Column, Integer, String, Enum
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

SQLALCHEMY_DATABASE_URL = "sqlite:///./sql_app.db"  # такие переменные лучше передавать в виде конфига
engine = create_engine(SQLALCHEMY_DATABASE_URL, connect_args={"check_same_thread": False})

Base = declarative_base()


class Fly(Base):
    __tablename__ = "fly"

    id = Column(Integer, primary_key=True, index=True)  # id
    # По идее вот это
    departure = Column(String)  # Город отправления
    #   и вот это можно вынести тоже в отдельные таблички, чтобы тоже не переиспользовать каждый раз
    arrival = Column(String)  # Город прибытия
    # это лучше передавать в виде datetime
    day = Column(Integer)  # День (число)
    # Компании лучше тогда выделить в отдельную табличку и сделать связь многие (полёты) к одному (компании)
    company = Column(String)  # Авиакомпания
    econom = Column(Integer)  # Тариф эконом
    busines = Column(Integer)  # Тариф бизнес


SessionLocal = sessionmaker(autoflush=False, bind=engine)  # Создание локальной сессии

# По идее будет нормально сделать такую вот тему
class Company(Base):
    __tablename__ = 'company'
    id = Column(Integer, primary_key=True, index=True)  # id
    name = Column(String)